import requests
from bs4 import BeautifulSoup as Bs


def get_html(url):
    """

    :param url: some url
    :return: r.text
    """
    r = requests.get(url)
    return r.text


def get_data(html):
    """

    :param html: some html code
    :return:
    """
    soup = Bs(html, 'lxml')
    div_list = []
    for i in soup.find_all('h1'):
        div_list.append(i.text)
    return div_list


def main():
    url = 'https://wordpress.org'
    for i in get_data(get_html(url)):
        print(i)


if __name__ == '__main__':
    main()
